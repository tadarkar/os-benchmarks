#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
vlan_to_link=$(sudo ip link | grep vlan | awk '{print $2}' | tr '@' ' ' | tr -d ':')
if_name=$(echo $vlan_to_link | awk '{print $2}')
if_hwinfo=$(sudo lshw -class network -businfo | grep $if_name | awk '{print substr($0, index($0, $4))}')

# Set up directory for results, delete if already exists
rm -rf results
mkdir results

# Install Packages
echo -n "Installing Packages "
date
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y
# Env Info packages
sudo apt-get install hwinfo numactl -y
# NPB build dependencies
sudo apt-get install gfortran -y
# DVFS dependencies
sudo apt-get install msr-tools cpufrequtils -y
# git
sudo apt-get install git
# fio
sudo apt-get install fio gdisk -y

# Build Stream
echo -n "Building STREAM - "
date
stream_ntimes=200
stream_array_size=50000000
stream_offset=0
stream_type=double
stream_optimization=O2
cd ./STREAM-separate
make clean
make NTIMES=$stream_ntimes STREAM_ARRAY_SIZE=$stream_array_size OFFSET=$stream_offset STREAM_TYPE=$stream_type OPT=$stream_optimization
cd ..

# Build NPB-CPU from source (MT)
echo -n "Building NPB CPU (MT) - "
date
cd ./NPB-CPUTests
rm -f bin/*
cp config/make-MT.def config/make.def
total_mem=$(sudo hwinfo --memory | grep "Memory Size" | awk '{print $3 $4}')
if [[ $(echo $total_mem | cut -d"G" -f 1) -lt 20 ]]; then
    cp config/suite-MT-lowmem.def config/suite.def
else
    cp config/suite-MT.def config/suite.def
fi
make clean
make suite
cd bin
mv bt.A.x bt.MT
mv cg.B.x cg.MT
mv ep.B.x ep.MT
mv ft.B.x ft.MT
if [[ ! $(echo $total_mem | cut -d"G" -f 1) -lt 20 ]]; then
    mv is.D.x is.MT
fi
mv lu.A.x lu.MT
mv mg.C.x mg.MT
mv sp.A.x sp.MT
mv ua.A.x ua.MT
cd ../..
