#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
arch=$(uname -m)
nsockets=1
if [ ${arch} == 'x86_64' ]; then
    nsockets=$(cat /proc/cpuinfo | grep "physical id" | sort -n | uniq | wc -l)
fi
nthreads=$(nproc --all)

#######################
### Parse Arguments ###
#######################
socket_num=-1
dvfs=-1
benchname=""
threading=""
while getopts ":n:d:b:t:s:u:" opt; do
    case ${opt} in
        n )
            socket_num=$OPTARG
            ;;
        d )
            dvfs=$OPTARG
            ;;
        b )
            benchname=$OPTARG
            ;;
        t )
            threading=$OPTARG
            ;;
        s )
            timstamp=$OPTARG
            ;;
        u )
            run_uuid=$OPTARG
            ;;
    esac
done

#####################
### Failure Modes ###
#####################
# Socket Number
if [ $socket_num -lt 0 ] || [ $socket_num -ge $nsockets ]; then
    echo "Bad Socket Number (socket_num=$socket_num)."
    exit 1
fi

# DVFS
if [[ ! "$dvfs" =~ ^(0|1)$ ]]; then
    echo "Bad DVFS Setting (dvfs=$dvfs)."
    exit 1
fi

# Benchmark Name
if [[ ! "$benchname" =~ ^(bt|cg|ep|ft|is|lu|mg|sp|ua)$ ]]; then
    echo "Bad Benchmark Name (benchname=$benchname)."
    exit 1
fi

# Threading
if [[ ! "$threading" =~ ^(ST|MT)$ ]]; then
    echo "Bad Threading Mode (threading=$threading)."
    exit 1
fi

###################
### Modify DVFS ###
###################
if [ ${dvfs} == 0 ]; then
    if [ ${arch} == 'x86_64' ] && [ -z $(lscpu | grep "Model name:" | grep -o -m 1 6142 | head -1) ]; then
        # Turn DVFS stuff off, re-run memory experiments
        dvfs_txt="no"
        dvfs_file="nodvfs"
        sudo modprobe msr
        oldgovernor=$(sudo cpufreq-info -p | awk '{print $3}')
        for (( n=0; n<=$((nthreads-1)); n++ ))
        do
            sudo wrmsr -p$n 0x1a0 0x4000850089
            sudo cpufreq-set -c $n -g performance
        done
    else
        echo "Unable to modify DVFS on current platform."
        exit 1
    fi
elif [ ${dvfs} == 1 ]; then
    dvfs_txt="yes"
    dvfs_file="dvfs"
fi

#####################
### NPB CPU Tests ###
#####################
cd ../NPB-CPUTests/bin
echo -n "Running NPB CPU Test $benchname (dvfs $dvfs_txt, socket $socket_num, $threading) - "
date
filename="npb.$benchname.socket${socket_num}.$dvfs_file.$threading"
numactl -N ${socket_num} ./$benchname.$threading > ~/$filename.out
sed '1,/nas.nasa.gov/d' ~/$filename.out | sed 's/ *, */,/g' | sed '/./,$!d' > ~/$filename.csv
sed -i '1s/$/,run_uuid,timestamp,nodeid,nodeuuid,socket_num,dvfs/' ~/$filename.csv
sed -i "2s/$/,$run_uuid,$timestamp,$nodeid,$nodeuuid,$socket_num,$dvfs_txt/" ~/$filename.csv
cut -d ',' -f 5 ~/$filename.csv | tail -1 >> ../../results/results.txt
mv ~/$filename.csv ../../results/
mv ~/$filename.out ../../results/

###################
### Revert DVFS ###
###################
if [ ${dvfs} == 0 ]; then
    for (( n=0; n<=$((nthreads-1)); n++ ))
    do
        sudo wrmsr -p$n 0x1a0 0x850089
        sudo cpufreq-set -c $n -g $oldgovernor
    done
fi
